"""
Check polarion ids.

Check if all polarion ids are different in a path.
"""

import argparse
import json
import logging
import pathlib
import sys
import typing
import uuid

formatter = logging.Formatter('%(message)s')
ch = logging.StreamHandler()
ch.setFormatter(formatter)
LOGGER = logging.getLogger(__name__)
LOGGER.addHandler(ch)


def is_uuid_valid(uuid_to_test: str, version: int = 4) -> bool:
    """Check if a string is a valid uuid."""
    try:
        uuid.UUID(uuid_to_test, version=version)
        return True
    except ValueError:
        return False


def get_report(raw_tests: typing.List[typing.Dict]) -> typing.Dict[str, typing.List[typing.Dict]]:
    """Generate the report."""
    result: typing.Dict[str, typing.List[typing.Dict]] = {}
    for test in raw_tests:
        if (polarion_id := test.get('id')):
            if polarion_id in result:
                result[polarion_id].append(test)
            else:
                result[polarion_id] = [test]

        else:
            LOGGER.debug('The test %s does not contain id', test.get('name', 'Undefined test'))

    return result


def format_error_msg(tests: typing.List[typing.Dict], error: str) -> str:
    """Format msg when an id is present in several tests."""
    msg: str = ''
    if error == 'invalid_id':
        msg = f'The id {tests[0]["id"]} is not a valid uuid:\n'
    elif error == 'duplicated_id':
        msg = f'The id {tests[0]["id"]} is in multiple tests:\n'
    for test in tests:
        maintainers = ', '. join(test['contact'])
        msg += f'\t- name: {test["name"]}, maintainer/s: {maintainers}\n'

    return msg


def main(args: typing.Optional[typing.List[str]] = None) -> int:
    """Command line to check all polarion ids."""
    description = 'Check if all polarion ids are different.'

    parser = argparse.ArgumentParser(description=description)

    parser.add_argument(
        '--input',
        '-i',
        type=pathlib.Path,
        required=True,
        help='JSON files with all tmt tests, you can use '
        '`tmt tests export --format json` to generate it.'
    )

    parser.add_argument(
        '--verbose',
        '-v',
        action='store_true',
        help='Enable verbosity'
    )

    parsed_args = parser.parse_args(args)

    if parsed_args.verbose:
        LOGGER.setLevel(logging.DEBUG)
    else:
        LOGGER.setLevel(logging.INFO)

    if not pathlib.Path(parsed_args.input).is_file():
        LOGGER.error('%s is not a file', parsed_args.input)
        return 1

    errors: bool = False

    raw_info = json.loads(parsed_args.input.read_text(encoding='utf-8'))

    LOGGER.info('Checking %i test/s\n', len(raw_info))

    report = get_report(raw_info)

    for polarion_id, tests in report.items():
        # Check for if a polarion id is used several times.
        if len(tests) > 1:
            LOGGER.error(format_error_msg(tests, error='duplicated_id'))
            errors = True
        else:
            LOGGER.debug('The polarion_id %s belongs to %s', polarion_id, tests[0]['name'])

        # Check the format of the id
        if not is_uuid_valid(polarion_id):
            errors = True
            LOGGER.error(format_error_msg(tests, error='invalid_id'))
        else:
            LOGGER.debug('The polarion id %s is a uuid id', polarion_id)

    if errors:
        LOGGER.error('Please review the log and fix the errors.')
        return 1

    LOGGER.info('All polarion ids are uniques and using a valid uuid format')
    return 0


if __name__ == "__main__":  # pragma: no cover
    sys.exit(main(sys.argv[1:]))

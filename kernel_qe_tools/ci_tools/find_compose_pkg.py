"""Find Compose Package."""

import argparse
import sys
import typing

import requests
from requests.exceptions import RequestException

JSON_HEADERS = {'Accept': 'application/json'}
SERVER = "http://download.eng.bos.redhat.com"
PRODUCT = "RHEL"


def usage():
    """Print usage information."""
    print("""
  Usage: find_compose_pkg.py -c <compose> -p <package(s)> [-v]

  Given a RHEL compose, this script will attempt to find the
  NVR of any package(s) provided.

  -c <compose> is mandatory.  If no -p <package(s)> is/are provided,
  then 'kernel' will be used by default.

  If -v is supplied, verbose output will be enabled, which will print
  all repositories the package is found in, including any arch
  limitations.  Without -v, just the first package name match will be
  printed (suitable for automation).

  The script will exit with:
    0 --> package(s) found in compose
    1 --> package(s) cannot be found in compose
    2 --> compose ID cannot be found
    3 --> any error fetching compose/package info
    4 --> incorrect usage
          """)


def find_pkg(pkg_list, compose_rpms, verbose):
    """Find all instances of each package in pkg_list in the compose."""
    found = False

    for pkg in pkg_list:
        for variant in compose_rpms:
            if variant not in ["BaseOS", "AppStream", "RT", "NFV", "HighAvailability",
                               "CRB", "Server", "Server-RT"]:
                continue

            pkg_nvr = ""
            pkg_arch = ""

            for arch in compose_rpms[variant]:
                variant_rpms = compose_rpms[variant][arch]
                for src in variant_rpms:
                    if pkg == '-'.join(src.split(':')[0].split('-')[:-1]):
                        pkg_nvr = '-'.join([pkg, src.split(':')[1].replace('.src', '')])
                        pkg_arch += ' ' + str(arch)
                        break

            pkg_arch = pkg_arch.strip().split()
            if len(pkg_arch) < 1:
                continue

            found = True
            if not verbose:
                print(pkg_nvr)
                break
            if len(pkg_arch) >= 4:
                print(variant + ': ' + pkg_nvr)
            else:
                print(variant + ': ' + pkg_nvr + ' ' + str(pkg_arch))

    return 0 if found else 1


def find_compose(compose):
    """Return compose info for the given compose ID."""
    major = compose.split('.')[0]
    try:
        url = (
            f"{SERVER}/{PRODUCT.lower()}-{major}/composes/{PRODUCT}-{major}/"
            f"{PRODUCT}-{compose}/compose/metadata/rpms.json"
        )
        response = requests.get(url, headers=JSON_HEADERS)
        response.raise_for_status()
        return True, response.json()
    except RequestException as err:
        return False, err


def rhel_package(compose, package, verbose):
    """Entrypoint to find each package in a given compose."""
    compose = compose.replace('RHEL-', '')
    major = compose.split('.')[0]
    pkg_list = {'kernel'} if not package else set(package)

    if len(compose) < 8 and len(compose.split('.')) <= 3:
        try:
            url = f"{SERVER}/released/{PRODUCT}-{major}/{compose}/metadata/rpms.json"
            response = requests.get(url, headers=JSON_HEADERS)
            response.raise_for_status()
            compose_info = response.json()

            if verbose:
                print(compose_info['payload']['compose']['id'])

            return find_pkg(pkg_list, compose_info['payload']['rpms'], verbose)
        except RequestException as err:
            if verbose:
                print(err)
            return 3

    found, compose_info = find_compose(compose)
    if found:
        return find_pkg(pkg_list, compose_info['payload']['rpms'], verbose)
    if verbose:
        print(f"Compose ID RHEL-{compose} not found")
    return 2


def main(args: typing.Optional[typing.List[str]] = None) -> int:
    """Command line interface for find_compose_pkg."""
    description = 'Find the package NVR for a package name within a RHEL compose'

    parser = argparse.ArgumentParser(description=description)

    parser.add_argument('-c', '--compose', help='RHEL compose ID')
    parser.add_argument('-p', '--package', help='RHEL package(s), may be provided multiple times',
                        action='append')
    parser.add_argument('-v', '--verbose', help='Enable verbose printing',
                        action='count', default=0)

    parsed_args = parser.parse_args(args)

    if parsed_args.compose is None:
        usage()
        return 4

    return rhel_package(parsed_args.compose, parsed_args.package, parsed_args.verbose)


if __name__ == "__main__":  # pragma: no cover
    sys.exit(main(sys.argv[1:]))

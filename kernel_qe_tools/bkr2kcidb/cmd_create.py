"""Subcommand create."""

import pathlib
import sys

from cki_lib import misc

from . import cmd_misc
from . import utils
from .dataclasses import ExternalOutputFile
from .dataclasses import ParserArguments
from .parser import Bkr2KCIDBParser


def build(cmds_parser, common_parser):
    """Build the argument parser for the create command."""
    cmd_parser, _ = cmd_misc.build(
        cmds_parser,
        common_parser,
        "create",
        help_message='Convert a Beaker XML results file into kcidb format.',
        add_subparser=False,
    )

    cmd_parser.description = 'Convert Beaker XML results file into kcidb format.'

    cmd_parser.add_argument('-c', '--checkout',
                            type=str,
                            required=True,
                            help="The checkout name used to generate all the kcidb ids.")

    cmd_parser.add_argument('--contact',
                            type=utils.email_type,
                            required=True,
                            action='append',
                            help='Contact email, "Full Name <username@domain>" and username@domain '
                                 'are accepted, this argument can be used several times.')

    cmd_parser.add_argument('--nvr',
                            type=str,
                            required=True,
                            help="NVR info.")

    cmd_parser.add_argument('-a', '--add-output-files',
                            nargs='+',
                            action=misc.StoreNameValuePair,
                            metavar="NAME=URL",
                            help="Add a list of output files to every test, the syntax is name=url "
                                 "(example: job_url=https://jenkins/job/my_job/1)"
                            )

    cmd_parser.add_argument('--brew-task-id',
                            type=str,
                            required=False,
                            help="Id of the Brew task where the package was compiled.")

    cmd_parser.add_argument('-d', '--debug',
                            action='store_true',
                            help="Enable it if using kernel debug build.")

    cmd_parser.add_argument('-i', '--input',
                            type=str,
                            default='beaker.xml',
                            help="Path to the Beaker XML file (By default beaker.xml).")

    cmd_parser.add_argument('-o', '--output',
                            type=str,
                            default='kcidb.json',
                            help="Path to the KCIDB file (By default kcidb.json).")

    cmd_parser.add_argument('--origin',
                            type=str,
                            default='bkr2kcidb',
                            help="The origin of the kenerl (By default bkr2kcidb).")

    cmd_parser.add_argument('-t', '--test_plan',
                            type=bool,
                            default=False,
                            help="Generate only test_plan.")

    cmd_parser.add_argument('--tests-provisioner-url',
                            type=str,
                            required=False,
                            help="URL of the tests provisioner, usually jenkins job url.")


def main(args):
    """Run cli command."""
    beaker_content = pathlib.Path(args.input).read_text(encoding='utf-8')
    if args.add_output_files:
        extra_output_files = [ExternalOutputFile(name=_name, url=_url)
                              for _name, _url in args.add_output_files.items()]
    else:
        extra_output_files = []
    package_name, package_version = utils.get_nvr_info(args.nvr)
    if not package_name or not package_version:
        sys.exit(f'Invalid value for nvr: {args.nvr}')
    external_arguments = ParserArguments(
        checkout=args.checkout,
        origin=args.origin,
        test_plan=args.test_plan,
        extra_output_files=extra_output_files,
        debug=args.debug,
        package_name=package_name,
        package_version=package_version,
        brew_task_id=args.brew_task_id,
        tests_provisioner_url=args.tests_provisioner_url,
        contacts=args.contact
    )
    bkr_parser = Bkr2KCIDBParser(beaker_content, external_arguments)
    bkr_parser.process()
    bkr_parser.write(args.output)
    print(f'File {args.output} wrote !!')

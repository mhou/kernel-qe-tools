"""Subcommand create."""

import configparser
import json
import os
import pathlib
import sys

from cki_lib.kcidb import ValidationError
from cki_lib.kcidb import validate_extended_kcidb_schema
from datawarehouse import Datawarehouse

from . import cmd_misc


def build(cmds_parser, common_parser):
    """Build the argument parser for the create command."""
    cmd_parser, _ = cmd_misc.build(
        cmds_parser,
        common_parser,
        "push2dw",
        help_message='Send kcidb results to the datawarehouse API.',
        add_subparser=False,
    )

    config_parser = configparser.ConfigParser()
    config_parser.read(('/etc/bkr2ckidb/config.ini',
                        os.path.expanduser('~/.config/bkr2ckidb/config.ini')))
    token = None
    if config_parser.has_section('dw'):
        token = config_parser.get('dw', 'token', fallback=None)
    # override from env
    token = os.environ.get('DW_TOKEN', token)

    cmd_parser.description = 'Send kcidb results to the datawarehouse API.'

    cmd_parser.add_argument('--token',
                            type=str,
                            required=token is None,
                            default=token,
                            help="Datawarehouse API token. "
                                 "Can also set via DW_TOKEN or config file.")

    cmd_parser.add_argument('-i', '--input',
                            type=pathlib.Path,
                            default=pathlib.Path('kcidb.json'),
                            help="The path to the kcidb file (By default kcidb.json).")

    cmd_parser.add_argument('--url',
                            type=str,
                            default='https://datawarehouse.cki-project.org',
                            help='Datawarehouse URL '
                                 '(By default https://datawarehouse.cki-project.org)')


def main(args):
    """Run cli command."""
    if not args.input.is_file():
        print(f'{args.input} is not a file or does not exist', file=sys.stderr)
        sys.exit(1)
    data = json.loads(args.input.read_text(encoding='utf-8'))
    try:
        print(f'Validating KCIDB format for {args.input} file')
        validate_extended_kcidb_schema(data)
    except ValidationError as exc:
        print(f'The file {args.input} is not a valid KCIDB file.', file=sys.stderr)
        print(exc, file=sys.stderr)
        sys.exit(1)
    try:
        dw_api = Datawarehouse(args.url, token=args.token)
        print(f'Sending KCIDB data to {args.url}')
        dw_api.kcidb.submit.create(data=data)
    # pylint: disable=broad-except
    except Exception as exc:
        print(f'Unable to send KCIDB data to {args.url}', file=sys.stderr)
        print(exc, file=sys.stderr)
        sys.exit(1)
    print('Done!')

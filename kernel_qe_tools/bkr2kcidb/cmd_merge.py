"""Subcommand merge."""

import json
import pathlib

from cki_lib.kcidb import KCIDBFile

from . import cmd_misc


def build(cmds_parser, common_parser):
    """Build the argument parser for the merge command."""
    cmd_parser, _ = cmd_misc.build(
        cmds_parser,
        common_parser,
        "merge",
        help_message='Merge multiple kcidb files.',
        add_subparser=False,
    )

    cmd_parser.description = 'Merge multiple kcidb files.'

    cmd_parser.add_argument('-r', '--result',
                            type=str,
                            required=True,
                            action='append',
                            help="Path to a source result (kcidb format).")

    cmd_parser.add_argument('-o', '--output',
                            type=str,
                            default='merged_kcidb.json',
                            help="Path to the merged KCIDB file (By default merge_kcidb.json).")


def main(args):
    """Run cli command."""
    merged_file = KCIDBFile(pathlib.Path(args.output))
    for result in args.result:
        data = json.loads(pathlib.Path(result).read_text(encoding='utf-8'))
        # Adding checkouts
        for kcidb_checkout in data['checkouts']:
            merged_file.set_checkout(kcidb_checkout['id'], kcidb_checkout)
        # Adding builds
        for kcidb_build in data['builds']:
            merged_file.set_build(kcidb_build['id'], kcidb_build)
        # Adding tests
        for kcidb_test in data['tests']:
            merged_file.set_test(kcidb_test['id'], kcidb_test)
    merged_file.save()
    print(f'File {args.output} wrote !!')

"""Cli module."""
import argparse
import sys

from . import cmd_create
from . import cmd_merge
from . import cmd_push2dw
from . import utils


def create_parser():
    """Create main parser."""
    description = "Beaker to KCIBD Tool"
    parser = argparse.ArgumentParser(description=description, add_help=False)
    parser.add_argument(
        '-h', '--help',
        default=True,
        nargs='?',
        const=True,
        help="Show help and exit"
    )
    return parser


def main(args=None):
    """Run commands."""
    common_parser = argparse.ArgumentParser(add_help=False)
    parser = create_parser()
    cmds_parser = parser.add_subparsers(title="Command", dest="command")
    cmd_create.build(cmds_parser, common_parser)
    cmd_merge.build(cmds_parser, common_parser)
    cmd_push2dw.build(cmds_parser, common_parser)

    parsed_args = parser.parse_args(args)

    commands = {
        'help': [parser.print_help],
        'create': [cmd_create.main, parsed_args],
        'merge': [cmd_merge.main, parsed_args],
        'push2dw': [cmd_push2dw.main, parsed_args],
    }

    command_name = parsed_args.command if parsed_args.command is not None else "help"
    function, *function_args = commands[command_name]
    try:
        function(*function_args)
    except utils.ActionNotFound as exc:
        print(f'{exc}', file=sys.stderr)
        cmds_parser.choices[parsed_args.command].print_help(file=sys.stderr)
        sys.exit(1)

"""Test Parser."""

from importlib.resources import files
import json
import pathlib
import tempfile
import unittest
import xml.etree.ElementTree as ET

from freezegun import freeze_time

from kernel_qe_tools.bkr2kcidb.dataclasses import ExternalOutputFile
from kernel_qe_tools.bkr2kcidb.dataclasses import ParserArguments
from kernel_qe_tools.bkr2kcidb.parser import Bkr2KCIDBParser

ASSETS = files(__package__) / 'assets/bkr2kcidb'


class TestParser(unittest.TestCase):
    """Test Parser."""

    maxDiff = None

    def setUp(self):
        """Initialize tester."""
        self.bkr_content = pathlib.Path(ASSETS, 'beaker.xml').read_text(encoding='utf-8')
        self.args = ParserArguments(
            checkout='checkout_id',
            debug=False,
            origin='bkr2kcidb',
            test_plan=False,
            extra_output_files=[],
            package_name='kernel',
            package_version='5.14.0-162.2.1.164.el9',
            brew_task_id='47919042',
            tests_provisioner_url='https://jenkins/job/my_job/1',
            contacts=['John Doe <jdoe@redhat.com>']
        )
        self.parser = Bkr2KCIDBParser(self.bkr_content, self.args)

        self.parser.job_whiteboard = 'Job WhiteBoard'

        self.parser.recipe_info = {
            'arch': 'x86_64',
            'job_id': '1',
            'id': '1',
            'recipe_set_id': '1',
            'system': 'some_server',
            'whiteboard': 'Recipe WhiteBoard'
        }

    def test_init(self):
        """Dummy test for init."""
        self.assertEqual('bkr2kcidb:checkout_id', self.parser.checkout_id)
        self.assertCountEqual({
            'checkout': None,
            'builds': [],
            'tests': []
        }, self.parser.report)

    def test_build_id(self):
        """Test build id."""
        cases = (
            ('bkr2kcidb:checkout_id_x86_64_kernel', False),
            ('bkr2kcidb:checkout_id_x86_64_kernel_debug', True),
        )
        for index, (expected, debug) in enumerate(cases):
            with self.subTest(expected):
                self.args.debug = debug
                self.parser.add_build()
                self.assertEqual(self.parser.report['builds'][index]['id'], expected)

    def test_exist_build_by_arch(self):
        """Test exist build by arch function works."""
        self.assertFalse(self.parser.exist_build_by_arch())

        # Add one
        self.parser.add_build()

        # Check again
        self.assertTrue(self.parser.exist_build_by_arch())

    def test_add_build(self):
        """Check add_build function."""
        cases = (
            {
                'description': 'Build without debug',
                'debug': False,
                'expected': {
                    'id': 'bkr2kcidb:checkout_id_x86_64',
                    'origin': 'bkr2kcidb',
                    'checkout_id': 'bkr2kcidb:checkout_id',
                    'architecture': 'x86_64',
                    'valid': True,
                    'misc': {
                        'package_name': 'kernel',
                        'provenance': [
                            {
                                'function': 'executor',
                                # pylint: disable=line-too-long
                                'url': 'https://brewweb.engineering.redhat.com/brew/taskinfo?taskID=47919042',  # noqa: E501
                                'service_name': 'buildsystem'
                            },
                            {
                                'function': 'provisioner',
                                'url': 'https://jenkins/job/my_job/1',
                                'service_name': 'jenkins'
                            }
                        ]
                    }
                }
            },
            {
                'description': 'Build with debug',
                'debug': True,
                'expected': {
                    'id': 'bkr2kcidb:checkout_id_x86_64_debug',
                    'origin': 'bkr2kcidb',
                    'checkout_id': 'bkr2kcidb:checkout_id',
                    'architecture': 'x86_64',
                    'valid': True,
                    'misc': {
                        'package_name': 'kernel',
                        'provenance': [
                            {
                                'function': 'executor',
                                # pylint: disable=line-too-long
                                'url': 'https://brewweb.engineering.redhat.com/brew/taskinfo?taskID=47919042',  # noqa: E501
                                'service_name': 'buildsystem'
                            },
                            {
                                'function': 'provisioner',
                                'url': 'https://jenkins/job/my_job/1',
                                'service_name': 'jenkins'
                            }
                        ]
                    }
                }
            },

        )

        for case in cases:
            with self.subTest(case['description']):
                # Fixing subtest problem with setup, cleaning up the report
                # pylint: disable=protected-access
                self.parser._clean_report()
                # Before we don't have any build
                self.assertEqual(0, len(self.parser.report['builds']))

                # We insert one
                self.parser.add_build()
                self.assertEqual(1, len(self.parser.report['builds']))

                self.assertCountEqual(case['expected'], self.parser.report['builds'][0])

                # If we add twice the same build_id, we only store one
                self.parser.add_build()
                self.assertEqual(1, len(self.parser.report['builds']))

    def test_add_checkout(self):
        """Check add_checkout function."""
        expected_checkout = {
            'id': 'bkr2kcidb:checkout_id',
            'origin': 'bkr2kcidb',
            'tree_name': 'rhel-8.6',
            'valid': True,
            'misc': {
                'kernel_version': '5.14.0-162.2.1.164.el9'
            }
        }

        # Empty checkout
        self.assertIsNone(self.parser.report['checkout'])

        # Adding the checkout
        self.parser.add_checkout()

        self.assertCountEqual(expected_checkout, self.parser.report['checkout'])

    def test_get_output_files(self):
        """Check get_out_files classmethod."""
        without_logs = "<xml><logs></logs></xml>"

        logs = ET.fromstring(without_logs).find('logs')

        self.assertCountEqual([], Bkr2KCIDBParser.get_output_files(logs))

        with_logs = """
        <xml>
          <logs>
            <log name="bar.log" href="http://srv/bar.log"/>
            <log name="foo.log" href="http://srv/foo.log"/>
          </logs>
        </xml>
        """
        output_files = [
            {'name': 'bar.log', 'url': 'http://srv/bar.log'},
            {'name': 'foo.log', 'url': 'http://srv/foo.log'}
        ]

        logs = ET.fromstring(with_logs).find('logs')

        self.assertListEqual(output_files, Bkr2KCIDBParser.get_output_files(logs))

    def test_get_results(self):
        """Check get_results function."""
        xml_content_without_results = "<xml><results></results></xml>"

        xml_content_with_results = """
        <xml>
          <results>
            <result path="result_one" result="Pass">
              <logs>
                <log name="bar.log" href="http://srv/bar.log"/>
              </logs>
            </result>
            <result path="result_two" result="Fail">
              <logs>
                <log name="bar.log" href="http://srv/bar.log"/>
                <log name="foo.log" href="http://srv/foo.log"/>
              </logs>
            </result>
          </results>
        </xml>
        """

        expected_with_results = [
            {'id': 'test_id.1',
             'name': 'result_one',
             'status': 'PASS',
             'output_files': [
                 {'name': 'bar.log', 'url': 'http://srv/bar.log'}
                ]
             },
            {'id': 'test_id.2',
             'name': 'result_two',
             'status': 'FAIL',
             'output_files': [
                 {'name': 'bar.log', 'url': 'http://srv/bar.log'},
                 {'name': 'foo.log', 'url': 'http://srv/foo.log'}
                ]
             }
        ]

        xml_content_with_bkr_info_in_text = """
        <xml>
          <results>
            <result path="/" result="Warn">External Watchdog Expired</result>
          </results>
        </xml>
        """
        expected_with_bkr_info_in_text = [
            {'id': 'test_id.1',
             'name': 'External Watchdog Expired',
             'status': 'ERROR',
             },
        ]

        xml_content_with_kernel_panic = """
        <xml>
          <results>
            <result path="/" result="Panic">Kernel Panic</result>
          </results>
        </xml>
        """
        expected_with_kernel_panic = [
            {'id': 'test_id.1',
             'name': 'Kernel Panic',
             'status': 'FAIL',
             },
        ]

        xml_content_with_an_empty_path = """
        <xml>
         <results>
           <result path="" result="Pass">
           </result>
         </results>
        </xml>
        """

        expected_with_an_empty_path = [
            {'id': 'test_id.1',
             'name': 'UNDEFINED',
             'status': 'PASS',
             },
        ]

        expected_miss_status = [
            {'id': 'test_id.1',
             'name': 'External Watchdog Expired',
             'status': 'MISS',
             },
        ]

        test_without_miss = {
            'id': 'test_id',
            'status': 'SOME_STATUS'
        }

        test_with_miss = {
            'id': 'test_id',
            'status': 'MISS'
        }

        cases = (
            ('Without results', xml_content_without_results, [], test_without_miss),
            ('With results', xml_content_with_results, expected_with_results, test_without_miss),
            ('Result with Beaker info in text',
             xml_content_with_bkr_info_in_text, expected_with_bkr_info_in_text, test_without_miss),
            ('Result with a Kernel Panic',
             xml_content_with_kernel_panic, expected_with_kernel_panic, test_without_miss),
            ('A result with a empty path',
             xml_content_with_an_empty_path, expected_with_an_empty_path, test_without_miss
             ),
            ('Test status is MISS',
             xml_content_with_bkr_info_in_text, expected_miss_status, test_with_miss),
        )

        for description, xml_content, expected_results, test in cases:
            with self.subTest(description):
                results = ET.fromstring(xml_content).find('results')
                self.assertListEqual(expected_results, self.parser.get_results(test, results))

    def test_add_test_only_test_plan(self):
        """Check add_test method when it is a test_plan."""
        beaker_content = """
        <xml>
          <task name="task name" id="10"/>
        </xml>
        """
        self.parser.args.test_plan = True
        expected_test = {
            'id': 'bkr2kcidb:checkout_id_x86_64_kernel_bkr2kcidb_10',
            'origin': 'bkr2kcidb',
            'build_id': 'bkr2kcidb:checkout_id_x86_64_kernel',
            'comment': 'task name',
            'path': 'task_name',
            'environment': {
                'comment': 'some_server'
            }
        }
        task = ET.fromstring(beaker_content).find('task')

        # Tests empty
        self.assertEqual([], self.parser.report['tests'])

        self.parser.add_test(task)

        self.assertEqual(1, len(self.parser.report['tests']))
        self.assertDictEqual(expected_test, self.parser.report['tests'][0])

    def test_add_test_no_test_plan(self):
        """Check add_test method when it is not a test plan."""
        beaker_content = """
        <xml>
          <task name="task with results" role="None" version="1.0-11" id="1" result="Pass" status="Completed" avg_time="300" start_time="2022-04-09 00:36:26" finish_time="2022-04-09 00:37:39" duration="0:01:13">
            <logs>
              <log href="https://srv/recipes/1/tasks/1/logs/harness.log" name="harness.log"/>
              <log href="https://srv/recipes/1/tasks/1/logs/taskout.log" name="taskout.log"/>
              <log href="https://srv/recipes/1/tasks/1/logs/test.log" name="test.log"/>
              <log href="https://srv/recipes/1/tasks/1/logs/messages" name="messages"/>
            </logs>
            <results>
              <result path="result one" start_time="2022-04-09 00:37:37" score="None" result="Pass" id="1">None
                <logs>
                  <log href="https://srv/recipes/1/tasks/1/results/1/logs/dmesg.log" name="dmesg.log"/>
                  <log href="https://srv/recipes/1/tasks/1/results/1/logs/avc.log" name="avc.log"/>
                  <log href="https://srv/recipes/1/tasks/1/results/1/logs/resultoutputfile.log" name="resultoutputfile.log"/>
                </logs>
              </result>
            </results>
          </task>
          <task name="task without results" role="None" version="1.0-11" id="2" result="Fail" status="Completed" avg_time="300" start_time="2022-04-09 00:36:26" finish_time="2022-04-09 00:37:39" duration="0:01:13">
            <logs>
              <log href="https://srv/recipes/1/tasks/2/logs/harness.log" name="harness.log"/>
              <log href="https://srv/recipes/1/tasks/2/logs/taskout.log" name="taskout.log"/>
              <log href="https://srv/recipes/1/tasks/2/logs/test.log" name="test.log"/>
              <log href="https://srv/recipes/1/tasks/2/logs/messages" name="messages"/>
            </logs>
          </task>
        </xml>
        """  # noqa: E501
        tasks = ET.fromstring(beaker_content).findall('task')
        expected_with_results = {
            'id': 'bkr2kcidb:checkout_id_x86_64_kernel_bkr2kcidb_1',
            'origin': 'bkr2kcidb',
            'build_id': 'bkr2kcidb:checkout_id_x86_64_kernel',
            'comment': 'task with results',
            'path': 'task_with_results',
            'start_time': '2022-04-09T00:36:26+00:00',
            'duration': 73,
            'output_files': [
                {'name': 'harness.log', 'url': 'https://srv/recipes/1/tasks/1/logs/harness.log'},
                {'name': 'taskout.log', 'url': 'https://srv/recipes/1/tasks/1/logs/taskout.log'},
                {'name': 'test.log', 'url': 'https://srv/recipes/1/tasks/1/logs/test.log'},
                {'name': 'messages', 'url': 'https://srv/recipes/1/tasks/1/logs/messages'},
                {
                    'name': 'console.log',
                    'url': 'https://beaker.engineering.redhat.com/recipes/1/logs/console.log'
                },
            ],
            'status': 'PASS',
            'misc': {
                'results': [{
                    'id': 'bkr2kcidb:checkout_id_x86_64_kernel_bkr2kcidb_1.1',
                    'name': 'result one',
                    'status': 'PASS',
                    'output_files': [
                        {'name': 'dmesg.log',
                         'url': 'https://srv/recipes/1/tasks/1/results/1/logs/dmesg.log'
                         },
                        {'name': 'avc.log',
                         'url': 'https://srv/recipes/1/tasks/1/results/1/logs/avc.log'
                         },
                        {'name': 'resultoutputfile.log',
                         'url': 'https://srv/recipes/1/tasks/1/results/1/logs/resultoutputfile.log'
                         }
                    ]}
                ],
                'provenance': [
                    {
                        'function': 'executor',
                        'url': 'https://beaker.engineering.redhat.com/recipes/1',
                        'misc': {
                            'job_id': '1',
                            'job_whiteboard': 'Job WhiteBoard',
                            'recipe_id': '1',
                            'recipe_set_id': '1',
                            'recipe_whiteboard': 'Recipe WhiteBoard'
                        },
                        'service_name': 'beaker'
                    },
                    {
                        'function': 'provisioner',
                        'service_name': 'jenkins',
                        'url': 'https://jenkins/job/my_job/1'
                    }
                ],
                'maintainers': [
                    {
                        'name': 'John Doe',
                        'email': 'jdoe@redhat.com'
                    }
                ]
            },
            'environment': {
                'comment': 'some_server'
            }
        }
        expected_without_results = {
            'id': 'bkr2kcidb:checkout_id_x86_64_kernel_bkr2kcidb_2',
            'origin': 'bkr2kcidb',
            'build_id': 'bkr2kcidb:checkout_id_x86_64_kernel',
            'comment': 'task without results',
            'path': 'task_without_results',
            'start_time': '2022-04-09T00:36:26+00:00',
            'duration': 73,
            'output_files': [
                {'name': 'harness.log', 'url': 'https://srv/recipes/1/tasks/2/logs/harness.log'},
                {'name': 'taskout.log', 'url': 'https://srv/recipes/1/tasks/2/logs/taskout.log'},
                {'name': 'test.log', 'url': 'https://srv/recipes/1/tasks/2/logs/test.log'},
                {'name': 'messages', 'url': 'https://srv/recipes/1/tasks/2/logs/messages'},
                {
                    'name': 'console.log',
                    'url': 'https://beaker.engineering.redhat.com/recipes/1/logs/console.log'
                },

            ],
            'status': 'FAIL',
            'misc': {
                'provenance': [
                    {
                        'function': 'executor',
                        'url': 'https://beaker.engineering.redhat.com/recipes/1',
                        'misc': {
                            'job_id': '1',
                            'job_whiteboard': 'Job WhiteBoard',
                            'recipe_id': '1',
                            'recipe_set_id': '1',
                            'recipe_whiteboard': 'Recipe WhiteBoard'
                        },
                        'service_name': 'beaker'
                    },
                    {
                        'function': 'provisioner',
                        'service_name': 'jenkins',
                        'url': 'https://jenkins/job/my_job/1'
                    }
                ],
                'maintainers': [
                    {
                        'name': 'John Doe',
                        'email': 'jdoe@redhat.com'
                    }
                ]

            },
            'environment': {
                'comment': 'some_server'
            }
        }

        # Empty tests
        self.assertEqual([], self.parser.report['tests'])

        # Insert first tests
        self.parser.add_test(tasks[0])

        self.assertEqual(1, len(self.parser.report['tests']))
        self.assertDictEqual(expected_with_results, self.parser.report['tests'][0])

        # Insert second tests
        self.parser.add_test(tasks[1])

        self.assertEqual(2, len(self.parser.report['tests']))
        self.assertDictEqual(expected_without_results, self.parser.report['tests'][1])

    def test_process(self):
        """Check if we can process a beaker content."""
        # In the beaker.xml we'll obtain 1 build and 17 tests
        self.parser.args.test_plan = True
        self.parser.process()

        self.assertEqual(1, len(self.parser.report['builds']))
        self.assertEqual(17, len(self.parser.report['tests']))

        self.parser.args.test_plan = False
        self.parser.process()

        self.assertEqual(1, len(self.parser.report['builds']))
        self.assertEqual(17, len(self.parser.report['tests']))

    @freeze_time("2022-04-22 09:53:20", tz_offset=0)
    def test_write_no_test_plan(self):
        """Check write the final result."""
        output_file = 'kcidb.json'
        expected_kcidb_file = pathlib.Path(ASSETS, 'kcidb.json')
        self.parser.args.test_plan = False
        self.parser.args.extra_output_files = [
            ExternalOutputFile(name='job_url', url='https://jenkins/job/my_job/1'),
            ExternalOutputFile(name='brew_url', url='https://brew/some_url')
        ]
        self.parser.process()
        fake_file = pathlib.Path('/tmp/', 'kcidb_all.json')
        self.parser.write(fake_file)
        with tempfile.TemporaryDirectory() as tmpdir:
            kcidb_file = pathlib.Path(tmpdir, output_file)
            self.parser.write(kcidb_file)

            computed = json.loads(kcidb_file.read_text(encoding='utf-8'))
            expected = json.loads(expected_kcidb_file.read_text(encoding='utf-8'))

        for obj_type, expected_values in expected.items():
            # Skip version checking
            if obj_type == "version":
                continue

            self.assertEqual(expected_values, computed[obj_type])

    def test_get_tree_name(self):
        """Test to get the tree name using the distro field."""
        # Test with the final beaker file
        expected_tree_name = 'rhel-8.6'
        self.assertEqual(expected_tree_name, self.parser.get_tree_name())

        # Test with recpies without distro field
        bkr_content = """
        <recipeSet priority="Normal" response="ack" id="1">
            <recipe id="1" job_id="1" recipe_set_id="1" result="Pass" status="Completed"/>
            <recipe id="2" job_id="2" recipe_set_id="1" result="Pass" status="Completed"/>
        </recipeSet>
        """
        parser = Bkr2KCIDBParser(bkr_content, self.args)
        self.assertIsNone(parser.get_tree_name())

        # Test with recpies with distro field which does not match
        bkr_content = """
        <recipeSet priority="Normal" response="ack" id="1">
            <recipe id="1" job_id="1" recipe_set_id="1"/>
            <recipe id="2" job_id="2" recipe_set_id="1" distro="RHHEL-8.6.33333"/>
        </recipeSet>
        """
        parser = Bkr2KCIDBParser(bkr_content, self.args)
        self.assertIsNone(parser.get_tree_name())

    def test_get_appended_output_files(self):
        """Test get_appended_output_files."""
        # Without files should return an empty list
        expected = []
        self.assertEqual(expected, self.parser.get_extra_output_files())

        # Now with values
        self.parser.args.extra_output_files = [
            ExternalOutputFile(name='job_url', url='https://jenkins/job/my_job/1'),
            ExternalOutputFile(name='brew_url', url='https://brew/some_url')

        ]
        expected = [
            {'name': 'job_url', 'url': 'https://jenkins/job/my_job/1'},
            {'name': 'brew_url', 'url': 'https://brew/some_url'}
        ]
        self.assertEqual(expected, self.parser.get_extra_output_files())

    def test_check_tests_with_the_same_name(self):
        """We can have the same test in different recipes."""
        test_name = 'check-install'
        expected = 4
        self.parser.process()

        result = 0
        # Getting all the test with check_install name
        for test in self.parser.report['tests']:
            if test_name in test['path']:
                result += 1

        self.assertEqual(expected, result)

    def test_get_default_maintainers(self):
        """Ensure get_default_maintainers works."""
        with_name = 'John Doe <jdoe@redhat.com>'
        without_name = 'jdoe@redhat.com'
        expected_with_name = [{'name': 'John Doe', 'email': 'jdoe@redhat.com'}]
        expected_without_name = [{'name': 'jdoe@redhat.com', 'email': 'jdoe@redhat.com'}]

        cases = (
            ('Email and email', with_name, expected_with_name),
            ('Only with email', without_name, expected_without_name),
        )

        for (description, args_maintainers, expected) in cases:
            with self.subTest(description):
                self.parser.args.contacts = [args_maintainers]
                self.assertListEqual(expected, self.parser.get_default_maintainers())

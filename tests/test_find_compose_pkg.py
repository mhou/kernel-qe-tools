"""Test Find Compose Pkg"""

from importlib.resources import files
import io
import json
import pathlib
import unittest
from unittest import mock

import responses

from kernel_qe_tools.ci_tools import find_compose_pkg

ASSETS = files(__package__) / 'assets/find_compose_pkg'


class TestFindComposePkg(unittest.TestCase):
    """Test Find Compose Pkg."""

    def setUp(self):
        """Init class."""
        rhel_rpms_file = pathlib.Path(ASSETS, 'RHEL-8.8.0-20230411.3_rpms.json')
        rhel_rpms_content = rhel_rpms_file.read_text(encoding='utf-8')
        self.daily_compose = 'RHEL-8.8.0-20230411.3'
        self.released_compose = 'RHEL-8.8.0'
        daily_compose_rpms_url = (
            'http://download.eng.bos.redhat.com/rhel-8/composes/RHEL-8/'
            f'{self.daily_compose}/compose/metadata/rpms.json'
        )
        released_compose_rpms_url = (
            'http://download.eng.bos.redhat.com/released/RHEL-8/'
            '8.8.0/metadata/rpms.json'
        )
        responses.add(responses.GET, daily_compose_rpms_url, status=200,
                      json=json.loads(rhel_rpms_content))
        responses.add(responses.GET, released_compose_rpms_url, status=200,
                      json=json.loads(rhel_rpms_content))

    @responses.activate
    def test_rhel_package_stdout(self):
        """Ensure rhel package stdout works."""
        cases = (
            ('Get kernel from a daily compose', self.daily_compose,
             None, 'kernel-4.18.0-477.10.1.el8_8', False),
            ('Get kernel from a released compose', self.released_compose,
             None, 'kernel-4.18.0-477.10.1.el8_8', False),
            ('Get GCC from compose', self.daily_compose, ['gcc'], 'gcc-8.5.0-18.el8', False),
            ('Verbose output with specific package for that arch', self.daily_compose,
             ['rt-tests'], "AppStream: rt-tests-2.5-1.el8 ['x86_64']", True),
            ('Verbose without specific package for that arch', self.daily_compose,
             ['rtla'], 'AppStream: rtla-5.14.0-4.el8', True),
            ('Invalid released compose', 'RHEL-8.8.0-20230411.10', None,
             'Compose ID RHEL-8.8.0-20230411.10 not found', True),
        )

        for (description, compose, packages, expected_output, verbose) in cases:
            with self.subTest(description), mock.patch(
                'sys.stdout', new_callable=io.StringIO
            ) as mock_stdout:
                find_compose_pkg.rhel_package(compose, packages, verbose)
                self.assertIn(expected_output, mock_stdout.getvalue())

    @responses.activate
    def test_rhel_package_exit_code(self):
        """Ensure rhel package return the right exit code."""
        cases = (
            ('Package found', self.daily_compose, ['kernel'], True, 0),
            ('Package not found', self.daily_compose, ['foobar'], True, 1),
            ('Compose not found', 'RHEL-8.8.0-12345678', ['kernel'], True, 2),
            ('Bad release compose', 'RHEL-1.2', ['kernel'], True, 3),
            ('Bad daily compose', 'RHEL-7.9-99999999.0', ['kernel'], True, 2),
        )

        for (description, compose, packages, verbose, exit_code) in cases:
            with self.subTest(description):
                self.assertEqual(exit_code,
                                 find_compose_pkg.rhel_package(compose, packages, verbose)
                                 )

    @responses.activate
    @mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_find_compose_pkg_main_valid(self, mock_stdout):
        """Smoke test with a valid compose."""
        result = find_compose_pkg.main(['-c', self.released_compose, '-p', 'kernel-rt', '-v'])
        self.assertEqual(result, 0)
        self.assertIn("NFV: kernel-rt-4.18.0-477.10.1.rt7.274.el8_8 ['x86_64']",
                      mock_stdout.getvalue())
        self.assertIn("RT: kernel-rt-4.18.0-477.10.1.rt7.274.el8_8 ['x86_64']",
                      mock_stdout.getvalue())

    def test_find_compose_pkg_main_no_compose(self):
        """Ensure cli fails without compose."""
        result = find_compose_pkg.main(['-v'])
        self.assertEqual(result, 4)

"""Test cases for bkr2kcidb cli module."""

from importlib.resources import files
import io
import json
import os
from pathlib import Path
import tempfile
import unittest
from unittest import mock

from cki_lib.kcidb import ValidationError

from kernel_qe_tools.bkr2kcidb import cli

ASSETS = files(__package__) / 'assets/bkr2kcidb'


class TestCli(unittest.TestCase):
    """Tests for cli."""

    @mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_create_subcommand(self, stdout_mock):
        """Test create subcommand."""
        # Prepare test
        with tempfile.TemporaryDirectory() as tmp_dir:
            kcidb_file = Path(tmp_dir, 'kcidb.json')
            expected_message = f'File {kcidb_file.resolve()} wrote !!\n'
            args = (
                "create "
                f"--input {ASSETS}/beaker.xml "
                f"--output {kcidb_file.resolve()} "
                "--checkout checkout_id "
                "--origin bkr2kcidb "
                "--test_plan true "
                "--add-output-files extra_file=https://someserver/url "
                "--nvr kernel-5.14.0-300.el9 "
                "--brew-task-id 47919042 "
                "--tests-provisioner-url https://jenkins/job/my_job/1 "
                "--contact username@redhat.com "
            ).split()
            # Run code
            cli.main(args)
            self.assertTrue(kcidb_file.is_file())

            # Check statistics (1 checkout, 1 build and 17 tests)
            report = json.loads(kcidb_file.read_text(encoding='utf-8'))
            self.assertEqual(1, len(report['checkouts']))
            self.assertEqual(1, len(report['builds']))
            self.assertEqual(17, len(report['tests']))

            # Check STDOUT
            self.assertEqual(expected_message, stdout_mock.getvalue())

    @mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_create_subcommand_without_output_files(self, stdout_mock):
        """Test create subcommand without output files."""
        # Prepare test
        with tempfile.TemporaryDirectory() as tmp_dir:
            kcidb_file = Path(tmp_dir, 'kcidb.json')
            expected_message = f'File {kcidb_file.resolve()} wrote !!\n'
            args = (
                "create "
                f"--input {ASSETS}/beaker.xml "
                f"--output {kcidb_file.resolve()} "
                "--checkout checkout_id "
                "--origin bkr2kcidb "
                "--test_plan true "
                "--nvr kernel-5.14.0-300.el9 "
                "--brew-task-id 47919042 "
                "--tests-provisioner-url https://jenkins/job/my_job/1 "
                "--contact username@redhat.com "
            ).split()
            # Run code
            cli.main(args)
            self.assertTrue(kcidb_file.is_file())

            # Check statistics (1 checkout, 1 build and 17 tests)
            report = json.loads(kcidb_file.read_text(encoding='utf-8'))
            self.assertEqual(1, len(report['checkouts']))
            self.assertEqual(1, len(report['builds']))
            self.assertEqual(17, len(report['tests']))

            # Check STDOUT
            self.assertEqual(expected_message, stdout_mock.getvalue())

    def test_create_subcommand_invalid_nvr(self):
        """Check create subcommand with a bad nvr value."""
        nvr = 'kernel-bad'
        expected_message = f'Invalid value for nvr: {nvr}'
        args = (
            "create "
            f"--input {ASSETS}/beaker.xml "
            "--output /tmp/kcidb_all.json "
            "--checkout checkout_id "
            "--origin bkr2kcidb "
            "--test_plan true "
            "--add-output-files job_url=https://jenkins/job/my_job/1 "
            "--contact username@redhat.com "
            f"--nvr {nvr}"
        ).split()
        with self.assertRaises(SystemExit) as context:
            cli.main(args)
        self.assertIn(expected_message, str(context.exception))

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_create_subcommand_without_contact(self, stderr_mock):
        """Check create subcommand without contact."""
        expected_message = 'error: the following arguments are required: --contact'
        args = (
            "create "
            f"--input {ASSETS}/beaker.xml "
            "--output /tmp/kcidb_all.json "
            "--checkout checkout_id "
            "--origin bkr2kcidb "
            "--test_plan true "
            "--add-output-files job_url=https://jenkins/job/my_job/1 "
            "--nvr kernel-5.14.0-300.el9 "
        ).split()
        with self.assertRaises(SystemExit) as context:
            cli.main(args)
        self.assertEqual('2', str(context.exception))
        self.assertIn(expected_message, stderr_mock.getvalue())

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_create_subcommand_without_checkout(self, stderr_mock):
        """Check create subcommand without checkout."""
        expected_message = 'error: the following arguments are required: -c/--checkout'
        args = (
            "create "
            f"--input {ASSETS}/beaker.xml "
            "--output /tmp/kcidb_all.json "
            "--origin bkr2kcidb "
            "--test_plan true "
            "--add-output-files job_url=https://jenkins/job/my_job/1 "
            "--nvr kernel-5.14.0-300.el9 "
            "--contact username@redhat.com "
        ).split()
        with self.assertRaises(SystemExit) as context:
            cli.main(args)
        self.assertEqual('2', str(context.exception))
        self.assertIn(expected_message, stderr_mock.getvalue())

    @mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_merge_subcommand(self, stdout_mock):
        """Test create subcommand."""
        # Prepare test
        with tempfile.TemporaryDirectory() as tmp_dir:
            kcidb_file = Path(tmp_dir, 'my_merged_kcidb.json')
            expected_message = f'File {kcidb_file.resolve()} wrote !!'
            args = (
                "merge "
                f"--result {ASSETS}/kcidb_1.json "
                f"--result {ASSETS}/kcidb_2.json "
                f"--result {ASSETS}/kcidb_3.json "
                f"--output {kcidb_file.resolve()} "
            ).split()
            # Run code
            cli.main(args)
            self.assertTrue(kcidb_file.is_file())

            # Check statistics (1 checkout, 3 builds and 271 tests)
            report = json.loads(kcidb_file.read_text(encoding='utf-8'))
            self.assertEqual(2, len(report['checkouts']))
            self.assertEqual(3, len(report['builds']))
            self.assertEqual(6, len(report['tests']))

            # Check STDOUT
            self.assertIn(expected_message, stdout_mock.getvalue())

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_push2dw_non_existing_file(self, stderr_mock):
        """Check push2dw when the file does not exist."""
        file_name = 'this_file_should_not_exist.json'
        expected_message = f"{file_name} is not a file or does not exist"
        args = (
            "push2dw "
            f"--input {file_name} "
            "--token any_token"
        ).split()
        with self.assertRaises(SystemExit) as context:
            cli.main(args)
        self.assertEqual('1', str(context.exception))
        self.assertIn(expected_message, stderr_mock.getvalue())

    @mock.patch('kernel_qe_tools.bkr2kcidb.cmd_push2dw.validate_extended_kcidb_schema')
    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_push2dw_with_an_error_in_the_kcidb_valitation(self,
                                                           stderr_mock,
                                                           mock_validate_extended_kcidb_schema):
        """Check push2dw when the KCIDB validation fails."""
        file_name = f'{ASSETS}/kcidb_1.json'
        cmd_error_msg = f'The file {file_name} is not a valid KCIDB file'
        exc_error_msg = 'Some error'
        args = (
            "push2dw "
            f"--input {file_name} "
            "--token any_token"
        ).split()
        mock_validate_extended_kcidb_schema.side_effect = ValidationError(exc_error_msg)
        with self.assertRaises(SystemExit) as context:
            cli.main(args)
        self.assertEqual('1', str(context.exception))
        self.assertIn(cmd_error_msg, stderr_mock.getvalue())
        self.assertIn(exc_error_msg, stderr_mock.getvalue())

    @mock.patch('kernel_qe_tools.bkr2kcidb.cmd_push2dw.validate_extended_kcidb_schema',
                mock.Mock())
    @mock.patch('kernel_qe_tools.bkr2kcidb.cmd_push2dw.Datawarehouse')
    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_push2dw_with_an_error_with_datawarehouse(self, stderr_mock, mock_dw_api):
        """Check push2dw when we have an error with dw."""
        file_name = f'{ASSETS}/kcidb_1.json'
        dw_url = 'http:/dw.cki.org'
        cmd_error_msg = f'Unable to send KCIDB data to {dw_url}'
        exc_error_msg = 'Some error'
        args = (
            "push2dw "
            f"--input {file_name} "
            f"--url {dw_url} "
            "--token any_token"
        ).split()
        mock_dw_api.side_effect = Exception(exc_error_msg)
        with self.assertRaises(SystemExit) as context:
            cli.main(args)
        self.assertEqual('1', str(context.exception))
        self.assertIn(cmd_error_msg, stderr_mock.getvalue())
        self.assertIn(exc_error_msg, stderr_mock.getvalue())

    @mock.patch('kernel_qe_tools.bkr2kcidb.cmd_push2dw.validate_extended_kcidb_schema',
                mock.Mock())
    @mock.patch('kernel_qe_tools.bkr2kcidb.cmd_push2dw.Datawarehouse', mock.Mock())
    @mock.patch('sys.stdout', new_callable=io.StringIO)
    # also testing getting the token from the env
    @mock.patch.dict(os.environ, {"DW_TOKEN": "any_token"})
    def test_push2dw(self, stdout_mock):
        """Check push2dw when everything works."""
        file_name = f'{ASSETS}/kcidb_1.json'
        dw_url = 'http:/dw.cki.org'

        validation_msg = f'Validating KCIDB format for {file_name} file'
        sending_msg = f'Sending KCIDB data to {dw_url}'
        done_msg = 'Done!'
        args = (
            "push2dw "
            f"--input {file_name} "
            f"--url {dw_url} "
        ).split()
        cli.main(args)
        self.assertIn(validation_msg, stdout_mock.getvalue())
        self.assertIn(sending_msg, stdout_mock.getvalue())
        self.assertIn(done_msg, stdout_mock.getvalue())

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_invalid_subcommand(self, stderr_mock):
        """Check invalid subcommand."""
        expected_message = "error: argument command: invalid choice: 'any_subcommand_non_defined'"
        args = (
            "any_subcommand_non_defined "
        ).split()
        with self.assertRaises(SystemExit) as context:
            cli.main(args)
        self.assertEqual('2', str(context.exception))
        self.assertIn(expected_message, stderr_mock.getvalue())

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_missing_token(self, stderr_mock):
        """Check fail with missing token."""
        expected_message = "push2dw: error: the following arguments are required: --token"
        args = (
            "push2dw "
        ).split()
        with self.assertRaises(SystemExit) as context:
            cli.main(args)
        self.assertEqual('2', str(context.exception))
        self.assertIn(expected_message, stderr_mock.getvalue())

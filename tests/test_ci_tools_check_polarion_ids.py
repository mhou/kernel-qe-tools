"""Test Polarion ID."""
from importlib.resources import files
import io
import unittest
from unittest import mock

from kernel_qe_tools.ci_tools import check_polarion_ids

ASSETS = files(__package__) / 'assets/check_polarion_ids'

VALID_TEST_1 = {
    'id': '20354d7a-e4fe-47af-8ff6-187bca92f3f9',
    'name': 'test_1',
    'contact': [
        'John Doe <jdoe@redhat.com>'
        ]
    }

VALID_TEST_2 = {
    'id': '20354d7a-e4fe-47af-8aa6-187bca92f3f9',
    'name': 'test_2',
    'contact': [
        'John Doe <jdoe@redhat.com>'
        ]
    }

VALID_TEST_DUPLICATING_ID_1 = {
    'id': '20354d7a-e4fe-47af-8ff6-187bca92f3f9',
    'name': 'duplicated_id',
    'contact': [
        'John Doe <jdoe@redhat.com>',
        'John Smith <jsmith@redhat.com>'
        ]
    }

TEST_WITHOUT_ID = {
    'name': 'test_without_id',
    'contact': [
        'John Doe <jdoe@redhat.com>'
        ]

}

TEST_WITH_INVALID_ID = {
    'id': 'invalid_id',
    'name': 'invalid_id',
    'contact': [
        'John Doe <jdoe@redhat.com>'
        ]
}


class TestCheckPolarionId(unittest.TestCase):
    """Test check polarion id."""

    def test_is_uuid_valid(self):
        """Ensure is_uuid_valid is working."""
        cases = (
            ('Valid UUID', '20354d7a-e4fe-47af-8ff6-187bca92f3f9', True),
            ('Invalid UUID', '20354d7a-e4fe-47af', False),
        )
        for (description, uuid, expected) in cases:
            with self.subTest(description):
                self.assertEqual(check_polarion_ids.is_uuid_valid(uuid), expected)

    def test_get_report_with_only_one_id_per_test(self):
        """Test get report with only one id per test."""
        tests = [VALID_TEST_1]

        expected = {VALID_TEST_1['id']: [VALID_TEST_1]}

        self.assertDictEqual(check_polarion_ids.get_report(tests), expected)

    def test_get_report_with_a_test_without_id(self):
        """Test get report with a test without id."""
        tests = [VALID_TEST_1, TEST_WITHOUT_ID]

        expected_report = {VALID_TEST_1['id']: [VALID_TEST_1]}
        expected_log = 'The test test_without_id does not contain id'

        with self.assertLogs(level='DEBUG') as log:
            self.assertDictEqual(check_polarion_ids.get_report(tests), expected_report)
            self.assertEqual(1, len(log.output))
            self.assertIn(expected_log, log.output[0])

    def test_get_report_with_a_test_with_and_invalid_id(self):
        """Test get report with a test with an invalid id."""
        tests = [VALID_TEST_1, TEST_WITH_INVALID_ID]

        expected = {
            VALID_TEST_1['id']: [VALID_TEST_1],
            TEST_WITH_INVALID_ID['id']: [TEST_WITH_INVALID_ID]
        }

        self.assertDictEqual(check_polarion_ids.get_report(tests), expected)

    def test_get_report_duplicating_id(self):
        """Test get report with tests with the same id."""
        tests = [VALID_TEST_1, VALID_TEST_DUPLICATING_ID_1]

        expected = {
            VALID_TEST_1['id']: [VALID_TEST_1, VALID_TEST_DUPLICATING_ID_1]
        }

        self.assertDictEqual(check_polarion_ids.get_report(tests), expected)

    def test_format_error_when_the_id_is_duplicated(self):
        """Test format error msg when the id is duplicated."""
        tests = [VALID_TEST_1, VALID_TEST_DUPLICATING_ID_1]

        msg = check_polarion_ids.format_error_msg(tests, error='duplicated_id')

        self.assertIn('The id 20354d7a-e4fe-47af-8ff6-187bca92f3f9 is in multiple tests:', msg)
        self.assertIn('- name: test_1, maintainer/s: John Doe <jdoe@redhat.com>', msg)
        self.assertIn(
            '- name: duplicated_id, maintainer/s: John Doe <jdoe@redhat.com>, John Smith '
            '<jsmith@redhat.com>', msg)

    def test_format_error_when_the_id_is_invalid(self):
        """Test format error msg when the id is invalid."""
        tests = [TEST_WITH_INVALID_ID]

        msg = check_polarion_ids.format_error_msg(tests, error='invalid_id')

        self.assertIn('The id invalid_id is not a valid uuid:', msg)
        self.assertIn('- name: invalid_id, maintainer/s: John Doe <jdoe@redhat.com>', msg)

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_cli_without_input_file(self, stderr_mock):
        """Test CLI without input file."""
        expected_message = 'error: the following arguments are required: --input/-i'
        with self.assertRaises(SystemExit) as context:
            check_polarion_ids.main()
        self.assertEqual(2, context.exception.code)
        self.assertIn(expected_message, stderr_mock.getvalue())

    def test_cli_with_invalid_input_file(self):
        """Test CLI without with invalid input file."""
        file = 'non_existing_file.json'
        expected_message = f'{file} is not a file'
        expected_exit_code = 1
        args = (
            f"--input {ASSETS}/{file}"
        ).split()
        with self.assertLogs() as log:
            self.assertEqual(expected_exit_code, check_polarion_ids.main(args))
            self.assertEqual(1, len(log.output))
            self.assertIn(expected_message, log.output[0])

    def test_cli_with_errors(self):
        """Test CLI with json with errors in tests."""
        header_msg = 'Checking 4 test/s'
        invalid_id_msg = 'The id invalid_id is not a valid uuid:'
        duplicated_id_msg = 'The id 20354d7a-e4fe-47af-8ff6-187bca92f3f9 is in multiple tests:'
        generic_error_msg = 'Please review the log and fix the errors.'
        expected_exit_code = 1
        args = (
            f"--input {ASSETS}/tests_with_errors.json"
        ).split()
        with self.assertLogs() as log:
            self.assertEqual(expected_exit_code, check_polarion_ids.main(args))
            self.assertTrue(any(header_msg in line for line in log.output))
            self.assertTrue(any(invalid_id_msg in line for line in log.output))
            self.assertTrue(any(duplicated_id_msg in line for line in log.output))
            self.assertTrue(any(generic_error_msg in line for line in log.output))

    def test_cli_with_valid_tests(self):
        """Test CLI with json with valid tests."""
        header_msg = 'Checking 2 test/s'
        succesfully_msg = 'All polarion ids are uniques and using a valid uuid format'
        expected_exit_code = 0
        args = (
            f"--input {ASSETS}/valid_tests.json"
        ).split()
        with self.assertLogs() as log:
            self.assertEqual(expected_exit_code, check_polarion_ids.main(args))
            self.assertIn(header_msg, log.output[0])
            self.assertIn(succesfully_msg, log.output[1])

    def test_cli_with_valid_tests_with_debug(self):
        """Test CLI with json with valid tests."""
        header_msg = 'Checking 2 test/s'
        debug_msg_1 = 'The polarion_id 20354d7a-e4fe-47af-8ff6-187bca92f3f9 belongs to test_one'
        debug_msg_2 = 'The polarion id 20354d7a-e4fe-47af-8ff6-187bca92f3f9 is a uuid id'
        debug_msg_3 = 'The polarion_id 20354d7a-e4fe-47af-8ff6-187bca92fff9 belongs to test_two'
        debug_msg_4 = 'The polarion id 20354d7a-e4fe-47af-8ff6-187bca92fff9 is a uuid id'
        succesfully_msg = 'All polarion ids are uniques and using a valid uuid format'
        expected_exit_code = 0
        args = (
            f"--input {ASSETS}/valid_tests.json "
            "-v"
        ).split()
        with self.assertLogs(level='DEBUG') as log:
            self.assertEqual(expected_exit_code, check_polarion_ids.main(args))
            self.assertIn(header_msg, log.output[0])
            self.assertIn(debug_msg_1, log.output[1])
            self.assertIn(debug_msg_2, log.output[2])
            self.assertIn(debug_msg_3, log.output[3])
            self.assertIn(debug_msg_4, log.output[4])
            self.assertIn(succesfully_msg, log.output[5])

"""Test Clean Dict."""

from argparse import ArgumentTypeError
from importlib.resources import files
import pathlib
import unittest
from unittest import mock
import xml.etree.ElementTree as ET

from freezegun import freeze_time

from kernel_qe_tools.bkr2kcidb import utils

ASSETS = files(__package__) / 'assets/bkr2kcidb'


class TestUtils(unittest.TestCase):
    """Test Parser."""

    def test_clean_dict(self):
        """Check clean dict function."""
        some_empty_values = {'a': {}, 'b': [], 'c': None, 'd': '', 'e': 'some_value'}
        expected_some_empty_values = {'e': 'some_value'}
        all_keys_with_values = {
            'a': {'aa': 'bb'},
            'b': [1, 2],
            'c': 'foo',
            'd': 'some',
            'e': 'some'
        }
        cases = (
            ('Empty dict', {}, {}),
            ('Some empty values', some_empty_values, expected_some_empty_values),
            ('All non empty values', all_keys_with_values, all_keys_with_values),
            ('Keep boolean values', {'bool': False}, {'bool': False})
        )
        for description, original_dict, expected in cases:
            with self.subTest(description):
                self.assertCountEqual(expected, utils.clean_dict(original_dict))

    def test_sanitize_name(self):
        """Check sanitize name function."""

        # Replacing characters
        original_name = 'some/name '
        expected_name = 'some_name_'
        self.assertEqual(expected_name, utils.sanitize_name(original_name))

        # Starting with any special characters
        name_1 = ' name'
        name_2 = '/name'
        expected_name = 'name'
        self.assertEqual(expected_name, utils.sanitize_name(name_1))
        self.assertEqual(expected_name, utils.sanitize_name(name_2))

        # Remove one underscore when we have two continuous
        name_1 = 'name__'
        name_2 = 'name_/'  # blackslash will be tranformed to underscore
        name_3 = 'name _'  # space will be tranformed to underscore
        name_4 = 'name /'  # space will be tranformed to underscore
        expected_name = 'name_'
        self.assertEqual(expected_name, utils.sanitize_name(name_1))
        self.assertEqual(expected_name, utils.sanitize_name(name_2))
        self.assertEqual(expected_name, utils.sanitize_name(name_3))
        self.assertEqual(expected_name, utils.sanitize_name(name_4))

    def test_get_second(self):
        """Check get_second function."""
        time_str = '01:00:44'
        seconds = 3644
        self.assertEqual(seconds, utils.get_seconds(time_str))

        time_str = '00:00:00'
        seconds = 0
        self.assertEqual(seconds, utils.get_seconds(time_str))

    def test_get_duration(self):
        """Ensure get_duration works."""
        cases = (
            ('Test with 0 seconds', '00:00:00', 0),
            ('Test with less than one day', '10:00:44', 36044),
            ('Test with days', '2 days, 0:30:33', 174633),
            ('Test with weeks', '1 week, 0:30:33', 606633),
            ('Test with days and weeks', '1 week, 2 days, 10:00:21', 813621),
        )

        for (description, bkr_duration, expected) in cases:
            with self.subTest(description):
                self.assertEqual(expected, utils.get_duration(bkr_duration))

    def test_get_utc_datetime(self):
        """Check get_utc_datetime function."""
        datetime_str = '2022-04-09 00:37:39'
        expected_utc_time = '2022-04-09T00:37:39+00:00'

        self.assertEqual(expected_utc_time, utils.get_utc_datetime(datetime_str))

        # When we don't have any datetime in beaker, we send None
        mocked_utc_time = '2000-01-01T00:00:00'
        expected_utc_time = f'{mocked_utc_time}+00:00'
        with freeze_time(mocked_utc_time):
            self.assertEqual(expected_utc_time, utils.get_utc_datetime(None))

    def test_get_int(self):
        """Check get_int function."""
        text_to_int = '123'
        self.assertEqual(123, utils.get_int(text_to_int))

        # Invalid cases
        text_to_int = 'abc'
        self.assertIsNone(utils.get_int(text_to_int))
        text_to_int = None
        self.assertIsNone(utils.get_int(text_to_int))

    def test_get_nvr_info(self):
        """Check get_nvr_info."""
        cases = (
            ('Normal kernel', 'kernel-5.14.0-1.el9', 'kernel', '5.14.0-1.el9'),
            ('Kernel with debug', 'kernel-debug-5.14.0-1.el9', 'kernel', '5.14.0-1.el9'),
            ('Kernel RT', 'kernel-rt-4.18.0-372.9.1.rt7.166.el8', 'kernel-rt',
             '4.18.0-372.9.1.rt7.166.el8'),
            ('Kernel RT with debug', 'kernel-rt-debug-4.18.0-372.9.1.rt7.166.el8',
             'kernel-rt', '4.18.0-372.9.1.rt7.166.el8'),
            ('Kernel Automotive', 'kernel-automotive-5.14.0-364.325.el9iv',
             'kernel-automotive', '5.14.0-364.325.el9iv'),
            ('Kernel Automotive with debug', 'kernel-automotive-debug-5.14.0-364.325.el9iv',
             'kernel-automotive', '5.14.0-364.325.el9iv'),
            ('Kernel 64K', 'kernel-64k-5.14.0-1.el9', 'kernel-64k', '5.14.0-1.el9'),
            ('Kernel 64K with debug', 'kernel-64k-debug-5.14.0-1.el9', 'kernel-64k',
             '5.14.0-1.el9'),
            ('Invalid kernel version', 'kernel-foo5.14.0.164.el9', None, None),
            ('Userspace package', 'vim-8.2.2637-19.el9', 'vim', '8.2.2637-19.el9'),
            ('Userspace with dashes in name', 'rt-setup-2.1-4.el8', 'rt-setup', '2.1-4.el8'),
            ('A package without x.y.z version', 'vim-8.2', None, None),
            ('Unified RT Kernel', 'kernel-rt-5.14.0-399.el9', 'kernel-rt', '5.14.0-399.el9'),
            ('Unified RT Kernel with debug', 'kernel-rt-debug-5.14.0-399.el9',
             'kernel-rt', '5.14.0-399.el9'),
            ('Kpatch', 'kpatch-patch-4_18_0-305_103_1-1-3.el8_4', 'kpatch-patch-4_18_0-305_103_1',
             '1-3.el8_4'),
        )

        for description, nvr, exp_package_name, exp_kernel_version in cases:
            with self.subTest(description):
                package_name, kernel_version = utils.get_nvr_info(nvr)
                self.assertEqual(package_name, exp_package_name)
                self.assertEqual(kernel_version, exp_kernel_version)

    def test_get_console_log(self):
        """Test get_console_log_url."""
        recipe_id = '12557513'
        expected = 'https://beaker.engineering.redhat.com/recipes/12557513/logs/console.log'
        self.assertEqual(utils.get_console_log_url(recipe_id), expected)

    def test_get_recipe_url(self):
        """Test get_recipe_url."""
        recipe_id = '12557513'
        expected = 'https://beaker.engineering.redhat.com/recipes/12557513'
        self.assertEqual(utils.get_recipe_url(recipe_id), expected)

    def test_get_provenance_info(self):
        """Get provenance info."""
        cases = (
            {
                'description': 'Given an url and service_name',
                'function': 'executor',
                'url': 'https://some_server/url',
                'service_name': 'some_service',
                'misc': None,
                'expected': {
                    'function': 'executor',
                    'url': 'https://some_server/url',
                    'service_name': 'some_service'
                },
            },
            {
                'description': 'Given an url, service_name and misc',
                'function': 'executor',
                'url': 'https://some_server/url',
                'service_name': 'some_service',
                'misc': {'foo': 'bar'},
                'expected': {
                    'function': 'executor',
                    'url': 'https://some_server/url',
                    'service_name': 'some_service',
                    'misc': {'foo': 'bar'}
                },
            },
            {
                'description': 'Given an url without service_name',
                'function': 'executor',
                'url': 'https://some_server/url',
                'service_name': None,
                'misc': None,
                'expected': {
                    'function': 'executor',
                    'url': 'https://some_server/url'
                },
            },
            {
                'description': 'Without an url',
                'function': 'executor',
                'url': None,
                'service_name': 'some_service',
                'misc': None,
                'expected': {},
            },
        )

        for case in cases:
            with self.subTest(case['description']):
                self.assertDictEqual(utils.get_provenance_info(case['function'],
                                                               case['url'],
                                                               case['service_name'],
                                                               case['misc']),
                                     case['expected'])

    def test_valid_emails(self):
        """Test valid emails."""
        cases = (
            {
                'description': 'Full Name and address',
                'input': 'Full Name <username@redhat.com>',
                'output': 'Full Name <username@redhat.com>'
            },
            {
                'description': 'Only address',
                'input': 'username@redhat.com',
                'output': 'username@redhat.com',
            },
            {
                'description': 'Only address with dot in the lef part',
                'input': 'user.name@redhat.com',
                'output': 'user.name@redhat.com',
            },
            {
                'description': 'Only address with underscore in the lef part',
                'input': 'user_name@redhat.com',
                'output': 'user_name@redhat.com',
            },
            {
                'description': 'Only address with hyphen in the lef part',
                'input': 'user-name@redhat.com',
                'output': 'user-name@redhat.com',
            },
            {
                'description': 'Multiples characters',
                'input': 'us.er-na_me@redhat.com',
                'output': 'us.er-na_me@redhat.com',
            }
        )
        for case in cases:
            with self.subTest(case['description']):
                self.assertEqual(utils.email_type(case['input']),
                                 case['output'])

    def test_invalid_emails(self):
        """Test invalid emails"""
        cases = (
            {
                'description': 'Full Name without <',
                'input': 'Full Name username@redhat.com>'
            },
            {
                'description': 'Full Name without <',
                'input': 'Full Name username@redhat.com>'
            },
            {
                'description': 'Full Name without < and >',
                'input': 'Full Name username@redhat.com'
            },
            {
                'description': 'Email without @',
                'input': 'usernameredhat.com'
            },
            {
                'description': 'Email without domain',
                'input': 'username'
            },
            {
                'description': 'Email with bad domain',
                'input': 'username@redhat'
            }
        )
        for case in cases:
            with self.subTest(case['description']):
                self.assertRaises(ArgumentTypeError,
                                  utils.email_type,
                                  case['input'])

    @mock.patch('kernel_qe_tools.bkr2kcidb.utils.sanitize_kcidb_status')
    def test_get_test_status(self, mock_sanitize):
        """Test test status."""
        bkr_content = pathlib.Path(ASSETS, 'beaker_with_miss.xml').read_text(encoding='utf-8')
        root = ET.fromstring(bkr_content)
        tasks = root.findall('.//task')

        cases = (
            ('Task without External Watchdog', tasks[0], False),
            ('Task reaching the External Watchdog', tasks[1], False),
            ('Task without being executed with External Watchdog', tasks[2], True)
        )
        for description, task, is_task_missed in cases:
            with self.subTest(description):
                mock_sanitize.reset_mock()
                status = utils.sanitize_test_status(task)
                if is_task_missed:
                    self.assertEqual('MISS', status)
                    mock_sanitize.assert_not_called()
                else:
                    mock_sanitize.assert_called()

    def test_get_task_maintainers(self):
        """Ensure get_task_maintainers works."""
        bkr_content = \
            pathlib.Path(ASSETS, 'beaker_task_maintainers.xml').read_text(encoding='utf-8')
        root = ET.fromstring(bkr_content)
        tasks = root.findall('.//task')

        user_one = {'email': 'user_1@redhat.com', 'gitlab': 'user_1', 'name': 'User 1'}
        user_two = {'email': 'user_2@redhat.com', 'name': 'User 2'}
        cases = (
            ('A task without parameter', tasks[0], []),
            ('A task with parameter but with maintainer', tasks[1], []),
            ('A task with a maintainer', tasks[2], [user_one]),
            ('A task with maintainers', tasks[3], [user_one, user_two]),
            ('A task with a maintainer without email', tasks[4], []),
        )
        for description, task, expected in cases:
            with self.subTest(description):
                self.assertListEqual(expected, utils.get_tasks_maintainers(task))

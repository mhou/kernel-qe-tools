# Find Compose Package

## Getting started

This utility will locate and print the full package NVR for a requested
package name within a requested compose.

The following repositories within a compose are searched:

* BaseOS
* AppStream
* RT
* NFV
* HighAvailability
* CRB
* Server
* Server-RT

You may supply an exact compose ID, such as 'RHEL-9.2.0-20230414.17', to search
for the requested package name.  If instead a RHEL release identifier is given,
for example 'RHEL-9.2.0', then the compose ID of the released RHEL version will
be used.

You may supply the `--package` argument multiple times to search the
compose for more than one package name.

The script will exit with the following status codes:

* 0 --> package(s) found in compose
* 1 --> package(s) cannot be found in compose
* 2 --> compose ID cannot be found
* 3 --> any error fetching compose/package info
* 4 --> incorrect usage

## Usage

```bash
usage: find_compose_pkg.py [-h] [-c COMPOSE] [-p PACKAGE] [-v]

Find the package NVR for a package name within a RHEL compose

optional arguments:
  -h, --help            show this help message and exit
  -c COMPOSE, --compose COMPOSE
                        RHEL compose ID
  -p PACKAGE, --package PACKAGE
                        RHEL package(s), may be provided multiple times
  -v, --verbose         Enable verbose printing
```

## Testing

From the repository root, run tox tests:
`podman run --pull newer --rm -it --volume .:/code:Z quay.io/cki/cki-tools:production tox`

# bkr2kcidb

## Getting started

Parser to transform beaker results to kcidb format.

## Usage

Here are some main features:

### Create

Create a kcidb file from a beaker file, if task has a parameter with field `name` equals to
`MAINTAINERS` and the value can be a list of maintainers for the task. If this parameter is not
provided the default one provided in the CLI will be used.

The format to define maintainers is:

User name <user@emailaddress.com> / gitlab.com_username

Gitlab user is not mandatory and it needs the slash, if you want to multiples maintainers, you only
need to add a comma and define the next one, here is an example

User 1 <user_1@emailaddress.com> / user_1, User 2 <user_2@emailaddress.com> / user_2

Here is the subcommand help

```bash
$ bkr2kcidb create --help
usage: bkr2kcidb create [-h] -c CHECKOUT --contact CONTACT --nvr NVR [-a NAME=URL [NAME=URL ...]] [--brew-task-id BREW_TASK_ID] [-d] [-i INPUT] [-o OUTPUT]
                        [--origin ORIGIN] [-t TEST_PLAN] [--tests-provisioner-url TESTS_PROVISIONER_URL]

Convert Beaker XML results file into kcidb format.

optional arguments:
  -h, --help            show this help message and exit
  -c CHECKOUT, --checkout CHECKOUT
                        The checkout name used to generate all the kcidb ids.
  --contact CONTACT     Contact email, "Full Name <username@domain>" and username@domain are accepted, this argument can be used several times.
  --nvr NVR             NVR info.
  -a NAME=URL [NAME=URL ...], --add-output-files NAME=URL [NAME=URL ...]
                        Add a list of output files to every test, the syntax is name=url (example: job_url=https://jenkins/job/my_job/1)
  --brew-task-id BREW_TASK_ID
                        Id of the Brew task where the kernel was compiled.
  -d, --debug           Enable it if using kernel debug build.
  -i INPUT, --input INPUT
                        Path to the Beaker XML file (By default beaker.xml).
  -o OUTPUT, --output OUTPUT
                        Path to the KCIDB file (By default kcidb.json).
  --origin ORIGIN       The origin of the kenerl (By default bkr2kcidb).
  -t TEST_PLAN, --test_plan TEST_PLAN
                        Generate only test_plan.
  --tests-provisioner-url TESTS_PROVISIONER_URL
                        URL of the tests provisioner, usually jenkins job url.
```

### Merge

Merge multiple kcidb files, here is the subcommand help

```bash
bkr2kcidb merge --help
usage: bkr2kcidb merge [-h] -r RESULT [-o OUTPUT]

Merge multiple kcidb files.

options:
  -h, --help            show this help message and exit
  -r RESULT, --result RESULT
                        Path to a source result (kcidb format).
  -o OUTPUT, --output OUTPUT
                        Path to the merged KCIDB file (By default merge_kcidb.json).
```

### Push2dw

Send a kcidb file to datawarehouse trough the API.

```bash
$ bkr2kcidb push2dw --help
usage: bkr2kcidb push2dw [-h] --token TOKEN [-i INPUT] [--url URL]

Send kcidb results to the datawarehouse API.

optional arguments:
  -h, --help            show this help message and exit
  --token TOKEN         Token in Datawarehouse.
  -i INPUT, --input INPUT
                        The path to the kcidb file (By default kcidb.json).
  --url URL             Datawarehouse URL (By default https://datawarehouse.cki-project.org)
```

Token can be specified via `--token`, `DW_TOKEN` in the environment or
via a configuration file `(/etc/bkr2kcidb/config.ini,
~/.config/bkr2kcidb/config.ini)`, e.g.

```ini
[dw]
token = <your token>
```
